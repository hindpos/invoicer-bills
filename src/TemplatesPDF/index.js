import React from 'react'

import { Clean, CleanView } from './Clean'

export class PDFComponent extends React.Component {
  components = {
    Clean
  }

  render() {
    const TagName = this.components[this.props.tag]
    return <TagName />
  }
}

export const TemplatesPDF = {
  Clean: CleanView
}