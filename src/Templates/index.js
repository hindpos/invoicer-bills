import Condensed from './Condensed'
import Clean from './Clean'

const Templates = {
  Condensed,
  Clean
}

export default Templates