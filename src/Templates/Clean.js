import React, { Component } from 'react'

import Logo from '../img/Logo.png'

function getMonth (date) {
  return ['January', 'February', 'March', 'April', 'May', 'June', 'July',
  'August', 'September', 'October', 'November', 'December'][date.getMonth()]
}

const Invoicer = ({ invoicer }) => {
  return (
    <header className="text-uppercase pb-1" 
    style={{ padding: '140px' }}>
      <h1 className="mb-0">{ invoicer.name }</h1>
      <p className="mb-0" style={{ fontSize: '12px'}}>{ invoicer.desc }</p>
    </header>
  )
}

const Customer = ({ customer }) => {
  return (
    <div className="text-uppercase pb-3 text-right pt-5" 
    style={{ padding: '140px' }}>
      {/* <h5>{ name }</h5> */}
      <h3 className="mb-1">{ customer.name }</h3>
      <p className="mb-0">{ customer.desc }</p>
    </div>
  )
}


const Summary = ({ date, total, currency = '₹' }) => {
  return (
    <div className="d-flex pt-5" 
    style={{
      paddingLeft: '140px',
      paddingRight: '140px'
    }}>
      <div className="w-50">
        <h3>Invoice / { getMonth(date) }</h3>
        <p>{ `${getMonth(date)} ${date.getDate()}, ${date.getFullYear()}` }</p>
      </div>
      <div className="w-50 text-light bg-dark p-2">
        <span className="text-uppercase">Total</span>
        <h1 className="mb-0 pt-4 text-right display-4">{`${currency} ${total.toFixed(2)}`}</h1>
      </div>
    </div>
  )
}

const Table = ({table}) => {
  const Headers = table.define
                  .map(({ placeholder}, index) => {
                    if (index === 0) {
                      return (<th key={index} style={{ paddingLeft: '140px' }}>{ placeholder }</th>)
                    } else if (index < table.define.length - 1) {
                      return (<th key={index} className="p-4 text-center">{ placeholder }</th>)
                    }
                    return (<th key={index} className="py-3 text-right" style={{ paddingRight: '140px' }}>{ placeholder }</th>)
                  })
  const Rows = table.data
                .map((row, outIndex) => (
                  <tr key={outIndex} style={{ borderBottom: '2px solid #f0f3f5' }}>
                    {
                      table.define
                        .map(({ key }, index) => {
                          if (index === 0) {
                            return (<td key={index} className="py-4" style={{ paddingLeft: '140px', fontWeight: 600 }}>{ row[key] }</td>)
                          } else if (index < table.define.length - 1) {
                            return (<td key={index} className="p-4 text-center">{ row[key] }</td>)
                          }
                          return (<td key={index} className="py-4 text-right" style={{ paddingRight: '140px' }}>{ row[key] }</td>)
                        })
                    }
                  </tr>
                ))
  return (
    <div style={{ height: '350px'}}>
      <table className='w-100'>
        <tr className="text-uppercase" style={{ background: '#f0f3f5' }}>
          { Headers }
        </tr>
        { Rows }
      </table>
    </div>
  )
}


const Total = ({ subtotal, extraPercent = 0, extra = 0, currency = '₹' }) => {
  return (
    <div className="d-flex text-uppercase w-100">
      <div className="d-flex flex-grow-1 p-3" style={{ background: '#f0f3f5' }}>
        <div className="px-3">
          <p className="mb-0" style={{ fontSize: '12px'}}>Subtotal</p>
          <p className="mb-0">{`${currency} ${subtotal.toFixed(2)}`}</p>
        </div>
        <span className="px-2" style={{ fontSize: '25px'}}>+</span>
        <div className="px-3">
          <p className="mb-0" style={{ fontSize: '12px'}}>Extra Charges And Tax ({extraPercent}%)</p>
          <p className="mb-0">{`${currency} ${extra.toFixed(2)}`}</p>
        </div>
      </div>
      <div className="text-light bg-dark text-right py-2 pr-3"
      style={{
        paddingLeft: '60px'
      }}>
        <p className="mb-0">total</p>
        <h1 className="mb-0">{`${currency} ${(subtotal+extra).toFixed(2)}`}</h1>
      </div>
    </div>
  )
}

const Footer = ({notes, mail}) => {
  return (
    <div style={{ color: '#bdc4c9' }}>
      <p className="py-4" style={{ borderBottom: '2px solid #f0f3f5', fontSize: '10px' }}>
        {notes}
      </p>
      <div>
        <span>{mail.incoming}</span>
        <span className="px-5 text-dark">{mail.outgoing}</span>
      </div>
    </div>
  )
}

class Clean extends Component {
  render () {
    const table = computeTable(this.props.table)
    const subTotal = this.props.subTotal(table.data)
    const extra = this.props.extra(table.data)
    return (
      <div className="container-fluid px-0">
        <div style={{ background: '#f0f3f5' }}>
          <img className="float-right" style={{paddingRight: '140px', paddingTop: '20px', height: '60px'}} src={Logo} alt="Logo"/>
          <Invoicer invoicer={this.props.invoicer}/>
          <Customer customer={this.props.customer}/>       
          <Summary date={new Date(this.props.date)} total={subTotal+extra}/>
        </div>
        <Table table={table}/>
        <div style={{ margin: '140px' }}>
          <Total subtotal={subTotal} extra={extra} extraPercent={this.props.extraPercent}/>
          <Footer notes={this.props.notes} mail={{incoming : this.props.invoicer.email, outgoing: this.props.customer.email}} />
        </div>
      </div>
    )
  }
}

function computeTable (table) {
  const { define, data } = table
  const computableDefine = define.filter(d => !d.fundemental)
  for (let index = 0; index < computableDefine.length; index++) {
    const def = computableDefine[index]
    for (let index = 0; index < data.length; index++) {
      const row = data[index]
      row[def.key] = def.compute(row) 
    }
  }
  return { define, data }
}

Clean.defaultProps = {
  date: '12/12/2018',
  table: {
    data: [
      { item: 'Zyonate Plus Injection', price: 50, qty: 1000 },
      { item: 'Potassium Permanganate', price: 1000, qty: 9 },
      { item: 'Formaldehyde 5ml', price: 1000, qty: 7 },
      { item: 'Googles', price: 50, qty: 750 },
      { item: 'EnCore Hand Gloves', price: 200, qty: 75 }
    ],
    define: [
      {
        fundemental: true,
        key: 'item',
        placeholder: 'Item',
        width: 3
      },
      {
        fundemental: true,
        key: 'price',
        placeholder: 'Rate',
        width: 2,
        default: 0
      },
      {
        fundemental: true,
        key: 'qty',
        placeholder: 'Quantity',
        default: 1,
        width: 1
      },
      {
        fundemental: false,
        key: 'total',
        default: 0,
        placeholder: 'LineTotal',
        compute: data => data.price * data.qty,
        width: 1
      }
    ]
  },
  subTotal: (data) => data.map(row => row.total).reduce((a, c) => a+c),
  extra: (data) => 0,
  extraPercent: 0,
  invoicer: {
    name: 'Ma Sarada Medico',
    desc: '3 Harekrishnanagar, Howrah',
    email: 'masaradamedico@gmail.com'
  },
  customer: {
    name: 'Ma Sarada Eye Care',
    desc: '3 Harekrishnanagar, Howrah',
    email: 'masaradaeyecare@gmail.com'
  },
  notes: 'All funds shall be cleared by due date. Currency is INR'
}


export default Clean