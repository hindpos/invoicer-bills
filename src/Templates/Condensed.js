import React, { Component } from 'react'
import styled from 'styled-components'

import './Condensed.css'

export const Span = styled.span`
  display: block;
  font-size: 14px;
`

export const Box = styled.div`
  padding-top: 5px;
  padding-left: 10px;
  padding-bottom: 80px;
  border: 2px solid #eee;
`

const FirstRow = () => {
  const invoiceNumber = '#A1220'
  const invoiceSummary = [
    {
      index: 0,
      title: 'Total',
      description: '₹33500'
    },
    {
      index: 1,
      title: 'Tax',
      description: '₹3500'
    },
    {
      index: 2,
      title: 'Due Date',
      description: '04/07/19'
    },
    {
      index: 3,
      title: 'Place of Supply',
      description: 'West Bengal'
    }
  ]
  .map(({ title, description, index }) => (
    (index > 2) ?
    (<div className="text-center px-3 my-2">
      <h6 className="font-grey mb-1">{ title }</h6>
      <span>{ description }</span>
    </div>) : 
    (<div className="text-center pr-5 my-2 border-right-thin">
      <h6 className="font-grey mb-1">{ title }</h6>
      <span>{ description }</span>
    </div>)
  ))
  const companyDetails = [
    {
      name: 'Parul Exports LLP',
      address1: '23, Santa Cruz Towers',
      addres2: 'Navi Mumbai',
      address3: 'Maharashtra 400703',
      phone: '+91 22 5644 7720',
      email: 'parulexports.com',
      supportMail: 'support@parulexports.com',
      gstin: 'GSTIN 19AWDPB2878C1ZP'
    }
  ]
  .map(({ name, address1, addres2, address3, phone, email, supportMail, gstin }) => (
    <div className="p-4">
      <h5>{name}</h5>
      <Span>{address1}</Span>
      <Span>{addres2}</Span>
      <Span>{address3}</Span>
      <Span>{phone}</Span>
      <Span>{email}</Span>
      <Span>{supportMail}</Span>
      <Span>{gstin}</Span>
    </div>
  ))
  return (
    <div className="d-flex border-bottom-thin">
      <div className="w-75 border-right-thin">
        <header>
          <h1 className="display-2 font-weight-bold mb-0">Invoicer.</h1>
          <h5 className="mb-0"
          style={{ 
            marginTop: '-10px',
            marginLeft: '5px'
            }}>
          by Dhano Tree</h5>
        </header>
        <div
        className="d-flex justify-content-between mr-3 mt-4 border-bottom-bold font-grey">
          <h6>Invoice { invoiceNumber }</h6>
          <span>04/04/19</span>
        </div>
        <div className="d-flex justify-content-around w-100">
          { invoiceSummary }
        </div>
      </div>
      <div className="comppany">
        { companyDetails }
      </div>
    </div>
  )
}

const SecondRow = () => {
  const TableHeader = ['Tax Rate', 'Taxable Amount', 'CGST', 'SGST', 'IGST', 'Tax']
  .map((obj) => (
    <th className="p-2 py-3 font-weight-bolder font-grey" style={{ fontSize: '15px'}}>{obj}</th>
  ))
  const TableRow  = [
    {
      index: 0,
      rate: '5%',
      taxableAmount: '10000',
      cgst: '250',
      sgst: '250',
      igst: '0',
      tax: '500'
    },
    {
      index: 1,
      rate: '12%',
      taxableAmount: '10000',
      cgst: '600',
      sgst: '600',
      igst: '0',
      tax: '1200'
    },
    {
      index: 2,
      rate: '18%',
      taxableAmount: '10000',
      cgst: '900',
      sgst: '900',
      igst: '0',
      tax: '1800'
    },
    {
      index: 3,
      rate: '28%',
      taxableAmount: '0',
      cgst: '0',
      sgst: '0',
      igst: '0',
      tax: '0'
    },
  ]
  .map(({ index, rate, taxableAmount, cgst, sgst, igst, tax }) => (
    (index > 2) ?
    (<tr>
      <td className="pt-3 pb-2 px-2">{rate}</td>
      <td className="pt-3 pb-2 px-2">{ taxableAmount }</td>
      <td className="pt-3 pb-2 px-2">{ cgst }</td>
      <td className="pt-3 pb-2 px-2">{ sgst }</td>
      <td className="pt-3 pb-2 px-2">{ igst }</td>
      <td className="pt-3 pb-2 px-2">{ tax }</td>
    </tr>) :
    (<tr>
      <td className="pt-3 pb-2 px-2 border-bottom-thin">{rate}</td>
      <td className="pt-3 pb-2 px-2 border-bottom-thin">{ taxableAmount }</td>
      <td className="pt-3 pb-2 px-2 border-bottom-thin">{ cgst }</td>
      <td className="pt-3 pb-2 px-2 border-bottom-thin">{ sgst }</td>
      <td className="pt-3 pb-2 px-2 border-bottom-thin">{ igst }</td>
      <td className="pt-3 pb-2 px-2 border-bottom-thin">{ tax }</td>
    </tr>) 
  ))
  const senderDetails = [
    {
      name: 'Rain Industries',
      address1: '23, Santa Cruz Towers',
      addres2: 'Navi Mumbai',
      address3: 'Maharashtra 400703',
      phone: '+91 22 5644 7720',
      email: 'parulexports.com',
      supportMail: 'support@parulexports.com',
      gstin: 'GSTIN 19AWDPB2878C1ZP'
    }
  ]
  .map(({ name, address1, addres2, address3, phone, email, supportMail, gstin }) => (
    <div>
      <h6>{name}</h6>
      <Span>{address1}</Span>
      <Span>{addres2}</Span>
      <Span>{address3}</Span>
      <Span>{phone}</Span>
      <Span>{email}</Span>
      <Span>{supportMail}</Span>
      <Span>{gstin}</Span>
    </div>
  ))
  return (
    <div className="d-flex">
      <div className="w-75 border-right-thin px-3">
        <table className="w-100">
          <tr className="border-bottom-thin">
            { TableHeader }
          </tr>
          { TableRow }
        </table>
      </div>
      <div className="sender p-4">
        <Span className="mb-2 font-weight-bold font-grey">BILL TO</Span>
        { senderDetails }
      </div>
    </div>
  )
}

const Table = () => {
  const TableHeader = [
    {
      index: 0,
      header: 'Item'
    },
    {
      index: 1,
      header: 'Quantity'
    },
    {
      index: 2,
      header: 'Price'
    },
    {
      index: 3,
      header: 'Discount'
    },
    {
      index: 4,
      header: 'Tax'
    },
    {
      index: 5,
      header: 'Linetotal'
    }
  ]
  .map(({ header, index }) => (
    (index > 4 ) ? 
    <th className="px-3 py-2 font-weight-bolder font-grey text-right">{ header }</th> :
    <th className="px-3 py-2 font-weight-bolder font-grey">{ header }</th>
  ))
  const TableRow = [
    {
      number: 1,
      item: 'Cement Bags 10L',
      quantity: '200',
      price: '125',
      discount: '20',
      tax: '5%',
      lineTotal: '10500'
    },
    {
      number: 2,
      item: 'Cement Bags 20L',
      quantity: '200',
      price: '125',
      discount: '20',
      tax: '18%',
      lineTotal: '11800'
    },
    {
      number: 3,
      item: 'Cement Bags 30L',
      quantity: '200',
      price: '125',
      discount: '20',
      tax: '12%',
      lineTotal: '11200'
    }
  ]
  .map(({ number, item, quantity, price, discount, tax, lineTotal }) => (
    <tr className="font-grey">
      {/* <td>{ number }</td> */}
      <th className="p-3 border-bottom-thin">{ item }</th>
      <th className="p-3 border-bottom-thin">{ quantity }</th>
      <th className="p-3 border-bottom-thin">{ price }</th>
      <th className="p-3 border-bottom-thin">{ discount }</th>
      <th className="p-3 border-bottom-thin">{ tax }</th>
      <th className="p-3 border-bottom-thin text-right">{ lineTotal }</th>
    </tr>
  ))
  return (
    <table className="mt-3 w-100">
      <tr style={{ background: '#363434'}}>{ TableHeader }</tr>
      { TableRow }
    </table>
  )
}

const TotalAmount = () => {
  const TableCal = [
    {
      index: 0,
      title: 'Tax Gst',
      amount: '3500'
    },
    {
      index: 1,
      title: 'Total',
      amount: '33500'
    },
    {
      index: 2,
      title: 'paid',
      amount: '0'
    },
    {
      index: 3,
      title: 'Amount Due',
      amount: '33500'
    }
  ]
  .map(({ index, title, amount }) => (
    (index === 0 || index === 2) ?
    (<tr className="font-grey">
      <th className="py-1 pr-5">{ title }</th>
      <th className="py-1 px-3 text-right">{ amount }</th>
    </tr>) :
    (<tr>
      <th className="py-1 pr-5">{ title }</th>
      <th className="py-1 px-3 text-right">{ amount }</th>
    </tr>)
  ))
  return (
    <div>
      <div className="d-flex justify-content-between border-bottom-thin">
        <div className="w-50"></div>
        <table className="w-50 mt-4 font-grey">
          <tr>
            <th className="py-3 pr-5">SUBTOTAL</th>
            <th className="p-3 text-right">30000</th>
          </tr>
        </table>
      </div>
      <div className="pt-3 d-flex justify-content-between">
        <div className="font-grey w-50">
          <h6 className="font-weight-bold">NOTES</h6>
          <p className="mb-0">Thank you very much for having business with us.</p>
          <p>Please send payments before due date.</p>
        </div>
        <table className="w-50 text-uppercase">
          { TableCal }
        </table>
      </div>
    </div>
  )
}

const Signature = () => {
  return (
    <div className="mt-3 d-flex justify-content-between font-grey">
      <Box className="w-50 mr-2">
        Authorised Signature & Stamp
      </Box>
      <Box className="w-50 ml-2">
        Receiver's Signature & Stamp
      </Box>
    </div>
  )
}

const Footer = () => {
  const accountNumber = 918010013715640
  const ifsc = 'UTIB0003004'
  return (
    <div className="mt-5 d-flex justify-content-between font-grey">
      <div>
        <p className="mb-0">Payment Details</p>
        <p>Account Number { accountNumber }  IFSC { ifsc }</p>
      </div>
      <p>Currency INR</p>
    </div>

  )
}

class Condensed extends Component {
  render () {
    return (
      <div className="container-fluid pt-4 px-5">
        <FirstRow />
        <SecondRow />
        <Table />
        <TotalAmount />
        <Signature />
        <Footer />
      </div>
    )
  }
}


export default Condensed