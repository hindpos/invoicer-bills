import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import Templates from './Templates'

const Home = () => {
  return (
    <div style={{
      textAlign: 'center',
      color: '#2c3e50',
      marginTop: '60px'
    }}>
      <h1>Invoice Templates</h1>
      <ul style={{listStyle: 'none', padding: 0}}>
        {Object.keys(Templates).map(t => (<li><Link to={`/${t}`}>{t}</Link></li>))}
      </ul>
    </div>
  )
}

class App extends Component {
  render() {
    return (
        <Router>
        <div>
          <Route path={`/`} component={Home} exact/>
          {Object.keys(Templates).map(t => (<Route path={`/${t}`} component={Templates[t]}/>))}
        </div>
      </Router>
    )
  }
}

export default App
